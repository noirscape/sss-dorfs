###   `Emma', 916253  
 
***

"I was near to my own Bed.  How incredibly interesting!"She is interested near her own fine Bed.  Within the last season, she was interested near her own fine Door.  She felt glum after getting into an argument.  She was interested near a fine Seat.  She felt euphoric due to inebriation.  She was annoyed after having a drink without using a goblet, cup or mug.  She felt glum after getting into an argument.  She was interested near a fine Door.  She was interested near a fine Table.  She was blissful after sleeping in a good bedroom.  She was annoyed at the lack of chairs.  
***

She is the daughter of Adil Braidcloister and Inod Feralcopper.  She is a worshipper of Togal the Strike of Turquoise, a casual worshipper of Datan, a casual worshipper of Ipa Fordlark the Mountainous Bear, a worshipper of Aned the Jewels of Chancing, an ardent worshipper of Uker Moistensplashed the Oceanic Seal, a worshipper of Ral the Diamond Guilds and a faithful worshipper of The Chances of Coincidence.  
***

She is a citizen of The Construct of Strapping.  She is a member of The Torch of Evening.  She is a former member of The Floor of Rinsing.  She arrived at Titthalbomrek on the 20th of Sandstone in the year 512.  
***

She is thirty-three years old, born on the 18th of Obsidian in the year 479.  
***

Her sideburns are clean-shaven.  Her very long moustache is arranged in double braids.  Her very long beard is braided.  Her very long hair is neatly combed.  She is average in size.  She has a prominent chin.  Her hair is burnt sienna.  Her sepia skin is slightly wrinkled.  Her eyes are ochre.  
***

She is quick to tire.  
***

`Emma' likes malachite, rose gold, milk opal, crystal glass, the color peach, bucklers, giant porcupines for their quills, the words of The South Ways and the sight of The Wisp of Glimmering.  When possible, she prefers to consume Longland beer.  She absolutely detests oysters.  
***

She has a great sense of empathy and a good spatial sense, but she has a shortage of patience.  
***

Like others in his culture, he holds craftsdwarfship to be of the highest ideals and celebrates talented artisans and their masterworks, greatly prizes loyalty, values family greatly, sees friendship as one of the finer things in life, believes that honesty is a high ideal, greatly respects artists and their works, really respects those that take the time to master a skill, deeply respects those that work hard at their labors, respects fair-dealing and fair-play, values cooperation, finds merrymaking and partying worthwhile activities, values martial prowess, values leisure time, respects commerce and finds nature somewhat disturbing.  He personally thinks the quest for knowledge is a delusional fantasy, has a negative view of those who exercise power over others, sees introspection as important and respects the law.  
***

He is grounded in reality.  He can handle stress.  He doesn't seek out excitement.  He generally acts impartially and is rarely moved to mercy, and he is disturbed by this as someone who dislikes those that seek to acquire power over others.  He is curious and eager to learn.  He has a tendency to consider ideas and abstractions over practical applications.  He is often nervous.  He tends to share his own experiences and thoughts with others.  He  is currently less private.  He does not easily hate or develop negative feelings.  He tends to be a bit stubborn in changing his mind about things.  He doesn't tend to hold on to grievances.  He  is currently more rude.  He  is currently more fearless.  He  is currently more confident.  He  is currently more shameless.  He  is currently more thoughtless.  She needs alcohol to get through the working day.  
***

Overall, Stinthäd is unfocused by unmet needs.  She is unfocused after being unable to pray to Togal the Strike of Turquoise.  She is unfocused after being unable to pray to Aned the Jewels of Chancing.  She is distracted after being unable to pray to Uker Moistensplashed the Oceanic Seal.  She is unfocused after being unable to pray to Ral the Diamond Guilds.  She is unfocused after being unable to pray to The Chances of Coincidence.  She is unfocused after being away from people.  She is unfocused after being unoccupied.  She is unfocused after doing nothing creative.  She is unfocused after being unable to acquire something.  She is level-headed after drinking.  She is unfocused after a lack of decent meals.  She is unfocused after being unable to fight.  She is unfettered after causing trouble.  She is unfettered after arguing.  She is unfocused after being unable to be extravagant.  She is unfocused after not learning anything.  She is unfocused after being unable to help anybody.  She is unfocused after a lack of abstract thinking.  She is unfocused after being unable to make merry.  She is unfettered after admiring art.  She is unfocused after being unable to practice a craft.  She is unfocused after being away from family.  She is unfocused after being away from friends.  She is unfocused after a lack of introspection.  She is unfocused after being unable to practice a martial art.  She is unfocused after being unable to practice a skill.  She is unfocused after being unable to take it easy.  
***

A short, sturdy creature fond of drink and industry. 
***

