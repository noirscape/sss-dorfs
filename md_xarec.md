###   `Xarec', Engraver  
 
***

"I finished up some work.  That was very satisfying!"He feels satisfied at work.  Within the last season, he felt satisfied upon improving engraving.  He felt euphoric due to inebriation.  He was annoyed after having a drink without using a goblet, cup or mug.  He felt satisfied at work.  He felt bitter after getting into an argument.  He was blissful after sleeping in a good bedroom.  He was annoyed at the lack of chairs.  He was interested near a fine Door.  He felt tenderness talking with the spouse.  
***

He is married to `Zeta' and has 4 children:  Id Copperclasped, Åblel Daggerstockades, Kivish Oarwhipped and Kel Boltsculpted.  He is the son of Degël Oilrulers and Udil Ropebegin.  
***

He is a citizen of The Construct of Strapping.  He is a member of The Torch of Evening.  He arrived at Titthalbomrek on the 20th of Sandstone in the year 512.  
***

He is fifty-six years old, born on the 23rd of Moonstone in the year 456.  
***

His hair is wavy.  His very long sideburns are braided.  His very long moustache is neatly combed.  His long beard is neatly combed.  His medium-length hair is neatly combed.  He has a low voice.  His ears are extraordinarily broad.  His eyebrows are somewhat high.  His sepia skin is slightly wrinkled.  His hair is burnt sienna.  His eyes are ochre.  His eyes are somewhat narrow.  
***

He is strong, but he is quick to tire, flimsy and clumsy.  
***

`Xarec' likes petrified wood, steel, variscite, birch wood, the color cardinal, shields, bins, yaks for their shaggy hair, scorpion brutes for their rhythmic undulations, the words of The South Ways, the sound of The Fancy Periwinkle and the sight of The Sienna Saturninity.  When possible, he prefers to consume anchovy, lettuces and barley wine.  He absolutely detests toads.  
***

He has a great sense of empathy, great intuition, a great feel for social relationships and a great deal of patience, but he has poor analytical abilities and a poor memory.  
***

Like others in his culture, he holds craftsdwarfship to be of the highest ideals and celebrates talented artisans and their masterworks, greatly prizes loyalty, sees friendship as one of the finer things in life, believes that honesty is a high ideal, greatly respects artists and their works, really respects those that take the time to master a skill, deeply respects those that work hard at their labors, respects fair-dealing and fair-play, values cooperation, finds merrymaking and partying worthwhile activities, values martial prowess, values leisure time, respects commerce, values knowledge and finds nature somewhat disturbing.  He personally values family and does not respect the law.  He dreams of crafting a masterwork someday.  
***

He is incredibly brave in the face of looming danger, perhaps a bit foolhardy.  He  is currently more fearless.  He accepts favors without developing a sense of obligation, preferring to act as the current situation demands.  He isn't particularly ambitious.  He could be considered rude.  He  is currently more rude.  He tries to keep his things orderly.  He is quick to form negative views about things.  He is stubborn.  He often acts with compassion.  He tends to be a bit stubborn in changing his mind about things.  He is slow to trust others.  He tends to be a little tight with resources when working on projects.  He has a greedy streak.  He  is currently more confident.  He  is currently more shameless.  He  is currently less private.  He  is currently more thoughtless.  When greeting others, he always smiles nervously.  He has a menacing stare when he's angry.  He needs alcohol to get through the working day.  
***

Overall, Logem is somewhat focused with satisfied needs.  He is not distracted after being away from people.  He is unfettered after staying occupied.  He is unfettered after doing something creative.  He is unfocused after leading an unexciting life.  He is unfocused after being unable to acquire something.  He is level-headed after drinking.  He is unfocused after a lack of decent meals.  He is unfocused after being unable to fight.  He is level-headed after causing trouble.  He is level-headed after arguing.  He is unfocused after being unable to be extravagant.  He is unfettered after learning something.  He is unfocused after being unable to help anybody.  He is unfocused after a lack of abstract thinking.  He is unfocused after being unable to make merry.  He is not distracted after being unable to admire art.  He is unfocused after being unable to practice a craft.  He is not distracted after being away from family.  He is unfocused after being away from friends.  He is unfocused after being unable to practice a martial art.  He is unfettered after practicing a skill.  He is unfocused after being unable to take it easy.  
***

A short, sturdy creature fond of drink and industry. 
***

