###   `junko', Tanner  
 
***

"I was near to my own Bed.  It's very interesting."She is interested near her own fine Bed.  She is interested near a fine Door.  Within the last season, she was blissful after sleeping in a good bedroom.  She felt tenderness talking with the spouse.  She was interested near a fine Table.  She felt euphoric due to inebriation.  She was annoyed after having a drink without using a goblet, cup or mug.  She felt satisfied after getting into an argument.  She was blissful dining in a great dining room.  She was annoyed at the lack of chairs.  She felt satisfied after creating an artifact.  She felt satisfied upon improving tanning.  She felt tenderness talking with a child.  
***

She is married to `FlimFlam69' and has 7 children:  Aban Blockadesaves, Ustuth Stockadebraided, Udib Gatepaints, `Lily', Eshtân Coalfoot, Endok Smiththeaters and Besmar Jadechannel.  She is the daughter of Unib Tombsrooted and Zefon Carryirons.  She is a worshipper of Togal the Strike of Turquoise, a casual worshipper of Datan, a worshipper of Aned the Jewels of Chancing, a worshipper of Ral the Diamond Guilds, a worshipper of Uker Moistensplashed the Oceanic Seal, a faithful worshipper of The Chances of Coincidence and a dubious worshipper of Ipa Fordlark the Mountainous Bear.  
***

She is a citizen of The Construct of Strapping.  She is a member of The Torch of Evening.  She is a former member of The Axe of Keepers.  She is a former member of The Vault of Tempests.  She arrived at Titthalbomrek on the 20th of Sandstone in the year 512.  
***

She is eighty years old, born on the 26th of Malachite in the year 432.  
***

She has a prominent chin.  Her lips are thick.  Her somewhat splayed out ears are somewhat narrow.  Her head is somewhat broad.  Her hair is burnt sienna with a touch of gray.  Her medium-length hair is neatly combed.  Her skin is sepia.  Her eyes are ochre.  
***

She is quick to tire.  
***

`junko' likes gabbro, fine pewter, gold opal, giant copperhead snake bone, cotton fabric, spears, gauntlets, hatch covers and the sight of The Sienna Saturninity.  When possible, she prefers to consume beet plants and sewer brew.  She absolutely detests bark scorpions.  
***

She has a great memory, a good intellect and a way with words, but she has a questionable spatial sense, bad intuition and quite poor focus.  
***

Like others in her culture, she holds craftsdwarfship to be of the highest ideals and celebrates talented artisans and their masterworks, greatly prizes loyalty, values family greatly, sees friendship as one of the finer things in life, greatly respects artists and their works, really respects those that take the time to master a skill, deeply respects those that work hard at their labors, respects fair-dealing and fair-play, values cooperation, finds merrymaking and partying worthwhile activities, values martial prowess, values leisure time, respects commerce, values knowledge and finds nature somewhat disturbing.  She personally believes it is important to conceal emotions and refrain from complaining, doesn't feel strongly about the law and does not particularly value the truth.  She dreams of creating a great work of art, and this dream was realized.  
***

She has no confidence at all in her talent and abilities.  She  is currently more confident.  She can get caught up in internal deliberations when action is necessary.  She  is currently more thoughtless.  She tends to avoid any physical confrontations, and she works to square this natural tendency with her respect of martial prowess.  She is somewhat fearful in the face of imminent danger.  She  is currently more fearless.  She doesn't mind a little tumult and discord in day-to-day living.  She generally acts impartially and is rarely moved to mercy.  She is curious and eager to learn.  She enjoys the company of others.  She has a calm demeanor.  She is not particularly interested in what others think of her.  She  is currently more shameless.  She tends not to reveal personal information.  She  is currently less private.  She is currently more rude.  She bites her nails when she's annoyed.  She needs alcohol to get through the working day.  
***

Overall, Udil is unfocused by unmet needs.  She is unfocused after being unable to pray to Togal the Strike of Turquoise.  She is unfocused after being unable to pray to Aned the Jewels of Chancing.  She is unfocused after being unable to pray to Ral the Diamond Guilds.  She is unfocused after being unable to pray to Uker Moistensplashed the Oceanic Seal.  She is unfocused after being unable to pray to The Chances of Coincidence.  She is level-headed after spending time with people.  She is not distracted after being unoccupied.  She is not distracted after doing nothing creative. She is unfocused after leading an unexciting life.  She is unfocused after being unable to acquire something.  She is untroubled after drinking.  She is unfocused after a lack of decent meals.  She is untroubled after causing trouble.  She is untroubled after arguing.  She is unfocused after being unable to be extravagant.  She is not distracted after not learning anything.  She is unfocused after being unable to help anybody.  She is unfocused after a lack of abstract thinking.  She is unfocused after being unable to make merry.  She is unfettered after admiring art.  She is not distracted after being unable to practice a craft.  She is level-headed after being with family.  She is unfocused after being away from friends.  She is unfocused after being unable to practice a martial art.  She is not distracted after being unable to practice a skill.  She is unfocused after being unable to take it easy.  
***

A short, sturdy creature fond of drink and industry. 
***

