###   `Garnet Sunset', expedition leader  
 
***

"I don't feel like I need to chase anything."Within the last season, she felt euphoric due to inebriation.  She was annoyed after having a drink without using a goblet, cup or mug.  She was blissful after sleeping in a good bedroom.  She felt euphoric due to inebriation.  She was annoyed when caught in the rain.  She felt satisfied after felling a tree.  She was annoyed at the lack of chairs.  She felt satisfied at work.  She didn't feel anything talking with a friend.  She was annoyed after sleeping on a rough cave floor.  
***

She is a worshipper of Uker Moistensplashed the Oceanic Seal.  
***

She is a citizen of The Construct of Strapping.  She is a member of The Torch of Evening.  She is the expedition leader of The Torch of Evening.  She is the broker of The Torch of Evening.  She is the bookkeeper of The Torch of Evening.  She arrived at Titthalbomrek on the 15th of Granite in the year 512.  
***

She is sixty-four years old, born on the 21st of Opal in the year 448.  
***

Her very long hair is tied in a pony tail.  She has a deeply recessed, narrow chin.  Her nose bridge is concave.  Her lips are thick.  She has a scratchy voice.  Her somewhat splayed out ears are very short.  Her ochre eyes are slightly wide-set.  Her eyelashes are quite long.  Her eyebrows are quite long.  Her hair is burnt sienna.  Her skin is sepia.  
***

She is very slow to tire and tough, but she is very weak.  
***

`Garnet Sunset' likes mica, lay pewter, blue garnet, backpacks, amulets, ballista arrows and the sound of The Incense of Toning.  When possible, she prefers to consume giant red squirrel, pig cheese and perry.  She absolutely detests cave spiders.  
***

She has a very good feel for social relationships, a good intellect, a feel for music and a good kinesthetic sense, but she has poor focus, poor creativity and little patience.  
***

Like others in her culture, she has a great deal of respect for the law, greatly prizes loyalty, values family greatly, sees friendship as one of the finer things in life, believes that honesty is a high ideal, greatly respects artists and their works, really respects those that take the time to master a skill, deeply respects those that work hard at their labors, respects fair-dealing and fair-play, values cooperation, values martial prowess, values leisure time, respects commerce, values knowledge and finds nature somewhat disturbing.  She personally values good craftsdwarfship and doesn't really value merrymaking.  She dreams of raising a family.  
***

She has a profound sense of duty and obligation.  She is not driven and rarely feels the need to pursue even a modest success.  She is prone to hatreds and often develops negative feelings.  She is very comfortable around others that are different from herself.  She is prone to strong feelings of lust.  She tends to ask others for help with difficult decisions.  She thinks she is fairly important in the grand scheme of things.  She tends to be a little tight with resources when working on projects.  She has a tendency toward forming deep emotional bonds with others.  She does not easily fall in love and rarely develops positive sentiments.  She sometimes acts with little determination and confidence.  She  is currently more confident.  She is not particularly interested in what others think of her.  She  is currently more shameless.  She  is currently more rude.  She  is currently more fearless.  She  is currently less private.  She  is currently more thoughtless.  She chews her lips when she gets excited.  She needs alcohol to get through the working day.  She likes working outdoors and grumbles only mildly at inclement weather.  
***

Overall, Ducim is unfocused by unmet needs.  She is not distracted after being away from people.  She is not distracted after being unoccupied.  She is unfocused after doing nothing creative.  She is unfocused after leading an unexciting life.  She is unfocused after being unable to acquire something.  She is untroubled after drinking.  She is unfocused after a lack of decent meals.  She is unfocused after being unable to fight.  She is unfocused after a lack of trouble-making.  She is unfocused after being unable to argue.  She is unfocused after being unable to be extravagant.  She is unfocused after not learning anything.  She is unfocused after being unable to help anybody.  She is unfocused after a lack of abstract thinking.  She is unfocused after being unable to admire art.  She is unfocused after being unable to practice a craft.  She is unfocused after being away from family.  She is unfocused after being away from friends.  She is unfocused after being unable to practice a martial art.  She is not distracted after being unable to practice a skill.  She is unfocused after being unable to take it easy.  She is unfocused after being unable to pray to Uker Moistensplashed the Oceanic Seal.  
***

A short, sturdy creature fond of drink and industry. 
***

