###   Bertenshed, "The Earthen Chance", a frilly water buffalo leather head veil  
 
***

This is a frilly water buffalo leather head veil.  All craftsdwarfship is of the highest quality.  It is encrusted with cushion rhyolite cabochons and decorated with water buffalo leather.  This object is adorned with hanging rings of water buffalo leather and menaces with spikes of mule leather.   
***

