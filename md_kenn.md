###   `kenn', Ranger  
 
***

"I finished up some work.  I am very satisfied."He feels satisfied at work.  
***

He is married to `Sentry' and has three children:  Lokum Ragclan, Tun Channelgullies and Sigun Singeddiamonds.  He is the son of Shem Palacegrouped and Kib Milebolt.  He is a casual worshipper of Nîles Furowl the Influence of Animals, a worshipper of Togal the Strike of Turquoise, an ardent worshipper of The Chances of Coincidence, a faithful worshipper of Uker Moistensplashed the Oceanic Seal, a casual worshipper of Aned the Jewels of Chancing and a casual worshipper of Datan.  
***

He is a citizen of The Construct of Strapping.  He is a member of The Torch of Evening.  He is a former member of The Superior Pillar.  He is a former member of The Humid Lantern.  He arrived at Titthalbomrek on the 7th of Malachite in the year 512.  
***

He is eighty-one years old, born on the 12th of Obsidian in the year 431.  
***

He has a recessed chin.  His sideburns are clean-shaven.  His very long moustache is arranged in double braids.  His long beard is arranged in double braids.  His hair is clean-shaven.  He has a scratchy voice.  His hair is burnt sienna with a touch of gray.  His skin is sepia.  His eyes are ochre.  His nose is slightly upturned.  
***

He is very strong and very agile, but he is very quick to tire.  
***

`kenn' likes alunite, fine pewter, levin opal, alpaca wool, bins, amulets, sheep for their wool, the words of The Flutes of Bewildering and the sight of The Silk of Luxury.  When possible, he prefers to consume chicory, two-grain wheat beer and limes.  He absolutely detests purring maggots.  
***

He has an amazing spatial sense, a good kinesthetic sense and a sum of patience, but he has poor empathy and a little difficulty with words.  
***

Like others in his culture, he holds craftsdwarfship to be of the highest ideals and celebrates talented artisans and their masterworks, has a great deal of respect for the law, greatly prizes loyalty, values family greatly, sees friendship as one of the finer things in life, believes that honesty is a high ideal, greatly respects artists and their works, really respects those that take the time to master a skill, respects fair-dealing and fair-play, values cooperation, finds merrymaking and partying worthwhile activities, values leisure time, respects commerce, values knowledge and finds nature somewhat disturbing.  He personally deeply respects skill at arms, values peace over war and doesn't really see the point of working hard.  He dreams of mastering a skill.  
***

He is intellectually stubborn, rarely changing his mind during a debate regardless of the merits.  He can handle stress.  He is not inherently proud of his talents and accomplishments.  He generally acts with a narrow focus on the current activity.  He likes a little excitement now and then.  He lives a fast-paced life.  He doesn't mind a little tumult and discord in day-to-day living.  He tends to avoid any physical confrontations, and he works to square this natural tendency with his respect of martial prowess.  He is not particularly interested in what others think of him.  He has a tendency toward forming deep emotional bonds with others.  He is slow to anger.  He has a greedy streak.  He rarely speaks when he's exasperated.  He scratches his ear when he's trying to remember something.  He needs alcohol to get through the working day.  
***

Overall, Ustuth is somewhat focused with satisfied needs.  He is not distracted after being unable to pray to Togal the Strike of Turquoise.  He is unfocused after being unable to pray to The Chances of Coincidence.  He is not distracted after being unable to pray to Uker Moistensplashed the Oceanic Seal.  He is not distracted after being away from people.  He is unfettered after staying occupied.  He is unfettered after doing something creative.  He is not distracted after leading an unexciting life.  He is not distracted after being unable to acquire something.  He is not distracted after being kept from alcohol.  He is not distracted after a lack of decent meals.  He is not distracted after a lack of trouble-making.  He is not distracted after being unable to argue.  He is not distracted after being unable to be extravagant.  He is not distracted after not learning anything.  He is not distracted after being unable to wander.  He is not distracted after being unable to help anybody.  He is not distracted after a lack of abstract thinking.  He is not distracted after being unable to make merry.  He is not distracted after being unable to admire art.  He is unfettered after practicing a craft.  He is not distracted after being away from family.  He is not distracted after being away from friends.  He is not distracted after being unable to practice a martial art.  He is unfettered after practicing a skill.  He is not distracted after being unable to take it easy.  
***

A short, sturdy creature fond of drink and industry. 
***

