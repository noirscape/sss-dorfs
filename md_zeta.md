###   `Zeta', Mason/Crafter  
 
***

"I've been alright."Within the last season, she felt euphoric due to inebriation.  She felt pleasure near a fine Door.  She was annoyed after having a drink without using a goblet, cup or mug.  She felt satisfied at work.  She felt satisfied at work.  She felt satisfied at work.  She felt pleasure near her own fine Bed.  She was blissful after sleeping in a good bedroom.  She felt pleasure after a satisfying acquisition.  She was content after putting on a finely-crafted item.  She was annoyed at the lack of chairs.  She felt satisfied upon improving masonry.  She was aroused talking with the spouse.  
***

She is married to `Xarec' and has 4 children:  Id Copperclasped, Åblel Daggerstockades, Kivish Oarwhipped and Kel Boltsculpted.  She is the daughter of Olin Youthlenses and Tun Divedbasement.  She is a worshipper of Togal the Strike of Turquoise, a worshipper of Aned the Jewels of Chancing, a worshipper of Uker Moistensplashed the Oceanic Seal, a faithful worshipper of The Chances of Coincidence, a casual worshipper of Datan and a faithful worshipper of Ral the Diamond Guilds.  
***

She is a citizen of The Construct of Strapping.  She is a member of The Torch of Evening.  She is a former member of The Girder of Destiny.  She arrived at Titthalbomrek on the 20th of Sandstone in the year 512.  
***

She is fifty-eight years old, born on the 22nd of Moonstone in the year 454.  
***

She is incredibly skinny with a small build.  She has a scratchy voice.  Her hair is clean-shaven.  She has a deeply recessed chin.  Her ochre eyes are round.  Her hair is burnt sienna.  Her skin is sepia.  Her ears have small lobes.  
***

She is incredibly quick to heal, but she is clumsy, quite susceptible to disease and extremely quick to tire.  
***

`Zeta' likes malachite, nickel, banded agate, tower-cap wood, echidna leather, cavies for their adorable call and the sight of The Wisp of Glimmering.  When possible, she prefers to consume anchovy and carrot wine.  She absolutely detests leeches.  
***

She has a great ability to focus, a good feel for social relationships and good intuition, but she has meager creativity and a large deficit of willpower.  
***

Like others in her culture, she holds craftsdwarfship to be of the highest ideals and celebrates talented artisans and their masterworks, has a great deal of respect for the law, greatly prizes loyalty, values family greatly, sees friendship as one of the finer things in life, believes that honesty is a high ideal, greatly respects artists and their works, really respects those that take the time to master a skill, deeply respects those that work hard at their labors, respects fair-dealing and fair-play, finds merrymaking and partying worthwhile activities, values martial prowess, values leisure time, respects commerce, values knowledge and finds nature somewhat disturbing.  She personally finds the ideas of independence and freedom somewhat foolish, values romance and doesn't see cooperation as valuable.  She dreams of mastering a skill.  
***

She has little time for forgiveness and will generally seek retribution.  She dislikes obligations and will try to avoid being bound by them, though she is conflicted by this for more than one reason.  She almost never feels discouraged.  She is very polite and observes appropriate rules of decorum when possible.  She  is currently more rude.  She generally acts impartially and is rarely moved to mercy.  She can handle stress.  She does not easily fall in love and rarely develops positive sentiments, and she is troubled by this because she values romance.  She has a tendency to go it alone, without considering the advice of others.  She tends to think before acting.  She  is currently more thoughtless.  She has a greedy streak.  She finds helping others emotionally rewarding.  She  is currently more fearless.  She  is currently more confident.  She  is currently more shameless.  She  is currently less private.  She needs alcohol to get through the working day.  
***

Overall, Monom is untroubled by unmet needs.  She is unfocused after being unable to pray to Togal the Strike of Turquoise.  She is unfocused after being unable to pray to Aned the Jewels of Chancing.  She is unfocused after being unable to pray to Uker Moistensplashed the Oceanic Seal.  She is unfocused after being unable to pray to The Chances of Coincidence.  She is unfocused after being unable to pray to Ral the Diamond Guilds.  She is not distracted after being away from people.  She is level-headed after staying occupied.  She is level-headed after doing something creative.  She is unfocused after leading an unexciting life.  She is not distracted after being unable to acquire something.  She is unfettered after drinking.  She is unfocused after a lack of decent meals.  She is unfocused after being unable to fight.  She is unfocused after a lack of trouble-making.  She is unfocused after being unable to argue.  She is unfettered after being extravagant.  She is not distracted after not learning anything. She is unfocused after being unable to help anybody.  She is unfocused after a lack of abstract thinking.  She is unfocused after being unable to make merry.  She is unfettered after admiring art.  She is level-headed after practicing a craft.  She is unfocused after being away from family.  She is unfocused after being away from friends.  She is unfocused after being unable to practice a martial art.  She is level-headed after practicing a skill.  She is unfocused after being unable to take it easy.  She is not distracted after being unable to make romance.  
***

A short, sturdy creature fond of drink and industry. 
***

