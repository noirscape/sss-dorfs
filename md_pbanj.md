###   `pbanj', Farmer  
 
***

"Everything is so much easier when you just tell the truth."Within the last season, she was annoyed at the lack of chairs.  She felt bitter after getting into an argument.  She felt euphoric due to inebriation.  She was blissful after sleeping in a very good bedroom.  She was irritated when drowsy.  She was annoyed after having a drink without using a goblet, cup or mug.  She felt satisfied at work.  She felt satisfied at work.  She was annoyed after sleeping in the dirt.  She felt euphoric due to inebriation.  
***

She is romantically involved with `Zewia'.  She is a worshipper of Uker Moistensplashed the Oceanic Seal.  
***

She is a citizen of The Construct of Strapping.  She is a member of The Torch of Evening.  She arrived at Titthalbomrek on the 15th of Granite in the year 512.  
***

She is seventy-two years old, born on the 7th of Hematite in the year 440.  
***

She is scrawny.  Her nose bridge is concave.  Her lips are thick.  She has a scratchy voice.  Her nose is broad.  Her somewhat splayed out short ears are broad.  Her slightly rounded ochre eyes are wide-set.  Her very long hair is neatly combed.  Her eyelashes are extremely long.  Her eyebrows are somewhat high.  Her hair is burnt sienna.  Her skin is sepia.  
***

She is clumsy, weak and flimsy.  
***

`pbanj' likes realgar, gold, red spinel, saguaro rib wood wood, muskox horn, quivers, thrones, large gems, cows for their haunting moos, horseshoe crab men for their ability to hide in sand and the sound of The Incense of Toning.  When possible, she prefers to consume alpaca cheese and beetroot wine.  She absolutely detests leeches.  
***

She has a great ability to focus, a very good sense of empathy and a feel for music, but she has poor analytical abilities, a little difficulty with words, a questionable spatial sense and a poor memory.  
***

Like others in her culture, she holds craftsdwarfship to be of the highest ideals and celebrates talented artisans and their masterworks, has a great deal of respect for the law, greatly prizes loyalty, values family greatly, sees friendship as one of the finer things in life, believes that honesty is a high ideal, greatly respects artists and their works, really respects those that take the time to master a skill, respects fair-dealing and fair-play, values cooperation, finds merrymaking and partying worthwhile activities, values leisure time, respects commerce, values knowledge and finds nature somewhat disturbing.  She personally values hard work, sees sacrifice as wasteful and foolish and finds those that develop skill with weapons and fighting distasteful.  She dreams of crafting a masterwork someday.  
***

She has an overbearing personality.  She tends to be swayed by the emotions of others.  She is a friendly individual.  She is quite polite.  She  is currently more rude.  She prefers to present herself modestly.  She is somewhat uncomfortable around those that appear unusual or live differently from herself.  She can handle stress.  She can sometimes act without deliberation.  She  is currently more thoughtless.  She isn't particularly curious about the world.  She  is currently more fearless.  She  is currently more confident.  She  is currently more shameless.  She  is currently less private.  She needs alcohol to get through the working day.  She likes working outdoors and grumbles only mildly at inclement weather.  
***

Overall, Udil is unfocused by unmet needs.  She is unfocused after being away from people.  She is not distracted after being unoccupied.  She is unfocused after doing nothing creative.  She is unfocused after leading an unexciting life.  She is unfocused after being unable to acquire something.  She is untroubled after drinking.  She is unfocused after a lack of decent meals.  She is unfocused after being unable to fight.  She is level-headed after causing trouble.  She is unfocused after being unable to help anybody.  She is unfocused after a lack of abstract thinking.  She is unfocused after being unable to make merry.  She is unfocused after being unable to admire art.  She is unfocused after being unable to practice a craft.  She is unfocused after not learning anything.  She is unfocused after being away from family.  She is unfocused after being away from friends.  She is not distracted after being unable to practice a skill.  She is unfocused after being unable to take it easy.  She is unfocused after being unable to pray to Uker Moistensplashed the Oceanic Seal.  
***

A short, sturdy creature fond of drink and industry. 
***

