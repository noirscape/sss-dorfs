###   `HM892', Miner  
 
***

"We must be careful not to waste."Within the last season, he felt satisfied at work.  He felt euphoric due to inebriation.  He was annoyed after having a drink without using a goblet, cup or mug.  He felt satisfied at work.  He felt satisfied at work.  He was blissful after sleeping in a good bedroom.  He felt euphoric due to inebriation.  He was annoyed at the lack of chairs.  He felt satisfied upon improving mining.  He was annoyed after sleeping on a rough cave floor.  He felt satisfied at work.  
***

He is a worshipper of Aned the Jewels of Chancing.  
***

He is a citizen of The Construct of Strapping.  He is a member of The Torch of Evening.  He arrived at Titthalbomrek on the 15th of Granite in the year 512.  
***

He is eighty-two years old, born on the 25th of Malachite in the year 430.  
***


***

His ears are very splayed out.  He has a narrow chin.  His medium-length sideburns are neatly combed.  His medium-length moustache is neatly combed.  His very long beard is braided.  His long hair is arranged in double braids.  His lips are thick.  He has a scratchy voice.  His slightly wide-set ochre eyes are round. His eyebrows are quite long.  His sepia skin is slightly wrinkled.  His hair is burnt sienna with a touch of gray.  
***

He is quick to heal, but he is quick to tire and flimsy.  
***

`HM892' likes granite, platinum, fortification agate, giant vulture leather, the color goldenrod, crossbows, trousers, bracelets, buckets, reindeer for their large herds, black bullheads for their whiskers, the words of The South Ways and the sight of The Euphoric Sheen.  When possible, he prefers to consume char and single-grain wheat beer.  He absolutely detests snails.  
***

He has a lot of willpower, but he has a shortage of patience and a meager ability with social relationships.  
***

Like others in his culture, he holds craftsdwarfship to be of the highest ideals and celebrates talented artisans and their masterworks, has a great deal of respect for the law, values family greatly, sees friendship as one of the finer things in life, believes that honesty is a high ideal, greatly respects artists and their works, really respects those that take the time to master a skill, respects fair-dealing and fair-play, values cooperation, finds merrymaking and partying worthwhile activities, values martial prowess, values leisure time, respects commerce, values knowledge and finds nature somewhat disturbing.  He personally disdains loyalty, finds eloquence and artful speech off-putting, values hard work and sees perseverance in the face of adversity as bull-headed and foolish.  He dreams of creating a great work of art.  
***

He lives at a high-energy kinetic pace.  He feels a strong need to reciprocate any favor done for him.  He is slow to anger.  He has a sense of duty, though he still views loyalty in a dim light, at least in the abstract.  He is somewhat quarrelsome, and he is bothered by this since he values friendship.  He does not easily fall in love and rarely develops positive sentiments.  He is quite polite.  He  is currently more rude.  He is pleased by his own appearance and talents.  He tends to be a little tight with resources when working on projects.  He has a greedy streak.  He has an active imagination.  He has a tendency to consider ideas and abstractions over practical applications.  He generally acts with a narrow focus on the current activity.  He enjoys the company of others.  He  is currently more fearless.  He  is currently more confident.  He  is currently more shameless.  He  is currently less private.  He  is currently more thoughtless.  He always takes a deep breath whenever he is surprised.  He runs his fingers through his hair when he's trying to remember something.  When he's trying to remember something, he usually starts tapping his feet.  When he's thinking, his body becomes very still.  He needs alcohol to get through the working day.  He likes working outdoors and grumbles only mildly at inclement weather.  
***

Overall, Stinthäd is unfocused by unmet needs.  He is unfocused after being away from people.  He is unfettered after staying occupied.  He is unfocused after doing nothing creative.  He is unfocused after leading an unexciting life.  He is unfocused after being unable to acquire something.  He is unfettered after drinking.  He is unfocused after a lack of decent meals.  He is unfocused after being unable to fight.  He is unfocused after a lack of trouble-making.  He is unfocused after being unable to argue.  He is unfocused after being unable to be extravagant.  He is not distracted after not learning anything.  He is unfocused after being unable to help anybody.  He is unfocused after a lack of abstract thinking.  He is unfocused after being unable to make merry.  He is unfocused after being unable to admire art.  He is unfocused after being unable to practice a craft.  He is unfocused after being away from family.  He is unfocused after being away from friends.  He is unfocused after being unable to practice a martial art.  He is unfettered after practicing a skill.  He is unfocused after being unable to take it easy.  He is unfocused after being unable to pray to Aned the Jewels of Chancing.  
***

A short, sturdy creature fond of drink and industry. 
***

